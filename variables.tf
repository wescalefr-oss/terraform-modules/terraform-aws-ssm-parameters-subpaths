variable "path" {
  type        = string
  description = "The SSM parameters path to query to retrieve the parameters from"
}

variable "profile" {
  type        = string
  description = "An AWS profile to use in the SSM parameters query (leave empty for no profile)"
  default     = ""
}