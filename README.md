# SSM Parameters by path module

This module provides a program to query a specified path in SSM parameter stores and retrieve as a map the found parameters names keyed by their names relative to the queried path    
It uses Terraform >= 0.12 for structure.
