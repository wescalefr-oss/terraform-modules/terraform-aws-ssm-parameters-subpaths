data "local_file" "program" {
  filename = "${path.module}/program.sh"
}

data "external" "names" {
  program     = [data.local_file.program.filename]
  working_dir = path.cwd
  query = {
    path    = var.path
    profile = var.profile
  }
}