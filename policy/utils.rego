package main

list_contains(list, elem) {
  list[_] = elem
}

# has_field returns whether an object has a field
has_field(object, field) {
  object[field]
}

# False is a tricky special case, as false responses would create an undefined document unless
# they are explicitly tested for
has_field(object, field) {
  object[field] == false
}

has_field(object, field) = false {
  not object[field]
  not object[field] == false
}

# TF specific

level_one(one, name) = node {
  node := [occurrence | occurrence := input[_][one][name]]
}

level_two(one, two, name) = node {
  node := [occurrence | occurrence := input[_][one][two][name]]
}

resource(type, name) = node {
  node := level_two("resource", type, name)
}

data(type, name) = node {
  node := level_two("data", type, name)
}

module(name) = node {
  node := level_one("module", name)
}

local(name) = node {
  node := level_one("locals", name)
}

variable(name) = node {
  node := level_one("variable", name)
}

output(name) = node {
  node := level_one("output", name)
}