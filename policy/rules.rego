package main

# Accessors

data_external_names := data("external", "names")

output_names := output("names")

variable_path := variable("path")

variable_profile := variable("profile")

# Rules

deny[msg] {
  count(data_external_names) != 1
  msg = "Define the names data external"
}

deny[msg] {
  count(output_names) != 1
  msg = "Define the names output"
}

deny[msg] {
  count(variable_path) != 1
  msg = "Define the path variable"
}

deny[msg] {
  count(variable_profile) != 1
  msg = "Define the profile variable"
}

deny[msg] {
  count([value | value := data_external_names[_].query.path; value == "${var.path}"]) != 1
  msg = "The names data external must use the path variable as query path input"
}

deny[msg] {
  count([value | value := data_external_names[_].query.profile; value == "${var.profile}"]) != 1
  msg = "The names data external must use the profile variable as query profile input"
}

deny[msg] {
  count([value | value := output_names[_].value; value == "${data.external.names.result}"]) != 1
  msg = "The names output must use the result of the names data external"
}