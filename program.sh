#!/bin/bash

set -e

eval "$(jq -r '@sh "QUERYPATH=\(.path) PROFILE=\(.profile)"')"
OUTPUT=$(aws ssm get-parameters-by-path ${PROFILE:+"--profile=${PROFILE}"} --path="${QUERYPATH}" | jq ".Parameters | to_entries | map({(.value.Name[${#QUERYPATH}+1:]):.value.Name}) | .[]" | jq -s -c -M add)
echo "$OUTPUT"