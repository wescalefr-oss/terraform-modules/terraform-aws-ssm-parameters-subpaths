output "names" {
  description = "Map of the name-keyed paths found by the SSM parameters query"
  value       = data.external.names.result
}